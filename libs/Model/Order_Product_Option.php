<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 12/3/15
 * Time: 12:16 PM
 */

namespace Model;


class Order_Product_Option extends \Emagid\Core\Model{
    public static $tablename = "order_product_option";

    public static $fields = [
        'order_product_id', 'data', 'price'
    ];

    public function getProduct()
    {
        $data = $this->getData();
        if(isset($data['optionalProduct']) && isset($data['optionalProduct']['product_map_id'])){
            return Product_Map::getItem($data['optionalProduct']['product_map_id'])->product();
        }

        return null;
    }

    public function getRingMaterial()
    {
        $data = $this->getData();
        if(isset($data['ringMaterial']) && isset($data['ringMaterial']['id'])){
            return Ring_Material::getItem($data['ringMaterial']['id']);
        }

        return null;
    }

    public function getColor()
    {
        $data = $this->getData();
        if(isset($data['color'])){
            return $data['color'];
        }

        return null;
    }

    public function getMetal()
    {
        $data = $this->getData();
        if(isset($data['metal'])){
            return $data['metal'];
        }

        return null;
    }

    public function getSize()
    {
        $data = $this->getData();
        if(isset($data['size'])){
            return $data['size'];
        }

        return null;
    }

    public function getShape()
    {
        $data = $this->getData();
        if(isset($data['shape'])){
            return strtoupper($data['shape']);
        }

        return null;
    }

    public function getStone()
    {
        $data = $this->getData();
        if(isset($data['stone'])){
            return strtoupper($data['stone']);
        }

        return null;
    }

    public function getData()
    {
        return json_decode($this->data, true);
    }
}