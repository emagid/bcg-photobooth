<?php

namespace Emagid\Mvc;

/**
 * View output visuals
 */
abstract class View
{

	/**
	* @var Array parameters passed to the constructor 
	*/
	var $_params; 


	public function __construct(){

		$this->_params = func_get_args();

	}
  
  public function __toString()
  {
    return (string)$this->_render();
  }
  
  abstract protected function _render();
}
