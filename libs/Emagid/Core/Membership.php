<?php 
namespace Emagid\Core ;

if (session_status() == PHP_SESSION_NONE) {
    \session_start();
}

/** 
* A helper class for all membership related
*/
class Membership {

	/**
	* 	Sets the session when a user has signed up .
	*
	*	@param  int/string  $id    the member's Id 
	*	@param  array       $roles array of roles the user is subscribed to .
	*	@param  object      $obj   any extra object 
	*/
	public static function setAuthenticationSession($id, $roles = [] , $obj = null ){
		$session = new \stdClass ; 

		$session->model = $obj ;
		$session->roles = $roles ; 
		$session->id = $id ; 

		$_SESSION['em_authentication_id'] = $id ;
		$_SESSION['em_authentication_model'] = $session ; 
	}

	/**
	* 	Gets the session when a user has signed up .
	*
	*	@return Object   The object that was stored when the user logged in . 
	*					- model  : the extra data
	*					- roles  : an array of roles the user belongs to. 
	*					- id 	 : the user's id .
	*/
	public static function getAuthenticationSession(){
		return $_SESSION['em_authentication_model'];
	}

	/**
	*	Delete the authentication session
	*/
	public static function destroyAuthenticationSession(){
		// check if session exists first so that it does not display error
		if(isset($_SESSION['em_authentication_id'])) {unset($_SESSION['em_authentication_id'] );}
		if(isset($_SESSION['em_authentication_model'])) {unset($_SESSION['em_authentication_model'] );}
	}


	/**
	* 	@return mixed the user id from the session 
	*/
	public static function userId(){
		if (!isset($_SESSION['em_authentication_id']))
			return null ;
		return $_SESSION['em_authentication_id'] ;
	}


	/** 
	* Check if user is authorized as per conditions provided .
	*
	* @param array $authorize
	* @return bool - whether the user is authorized or not .
	*/ 
	public static function isAuthorized($authorize){
		if (self::isInRoles($authorize['roles'])){
			return true;
		} else {
			return false;
		}
	}


	/**
	* 	Checks if user is logged in .
	*/
	public static function isAuthenticated(){
		
		return isset($_SESSION['em_authentication_id'] );
	}

	/**
	* 	Checks if user is assigned to a role, or roles
	*
	*	@param   Array   $params   List of roles to check 
	*	@return  bool              True / False if the user is in the roles in question 
	*/
	public static function isInRoles($params = []){
		if(!self::isAuthenticated())
			return false; 

		if(!count($params))
			return true;

        $params = flatten($params);

		$model = self::getAuthenticationSession();

		if(!$model->roles || !count($model->roles)){
			return false;
		}

		foreach ($params as $role) {
			if(in_array($role, $model->roles))
				return true;
		}

		return false; 
	}


	/**
 	*  @copyright eMagid 2014
   * Generates a password with a salt (uses default salt generation function if no salt provided)
   * using the chosen algo (sha256 default)
   * @param string $password
   * @param string $salt
   * @param string $algo
   * @return array Newly generated password with salt
   */
  public static function hash($password, $salt = null, $algo = null)
  {
    $algo = ($algo != null) ? $algo : 'sha256';
    $salt = ($salt != null) ? $salt : self::_generateSalt();
    
    return ['password' => hash_hmac($algo, $password, $salt), 'salt' => $salt];
  }
  
  /**
 	*  @copyright eMagid 2014
   * Generates a salt for passwords generation
   * @return string salt for password
   */
  protected static function _generateSalt()
  {
    return md5(time());
  }


}

?>