<?php
 if(count($model->contacts)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
            <th width="15%">Name</th>
            <th width="15%">Email</th>
            <th width="10%">Phone</th>
            <th width="20%">Form</th>
            <th width="20%">Donation Amount</th>
          <th width="15%" class="text-center">Edit</th>
          <th width="15%" class="text-center">Delete</th>	
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->contacts as $obj){ ?>
        <tr>
            <td><a href="<?php echo ADMIN_URL; ?>contacts/update/<?= $obj->id ?>"><?php echo $obj->name; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>contacts/update/<?= $obj->id ?>"><?php echo $obj->email; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>contacts/update/<?= $obj->id ?>"><?php echo $obj->phone; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>contacts/update/<?= $obj->id ?>"><?php echo \Model\Contact::$forms[$obj->form]; ?></a></td>
            <td><a href="<?php echo ADMIN_URL; ?>contacts/update/<?= $obj->id ?>">$<?php echo $obj->donation; ?></a></td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>contacts/update/<?= $obj->id ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>contacts/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'contacts';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>