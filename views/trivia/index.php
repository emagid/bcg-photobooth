<main>
    <? $questions = $model->questions?>
<a class='home' href="/home/kiosk1"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>
	<section class="product_page trivia_page" >

        <div class='background'></div>   
        <!-- trivia questions -->
        
        	<section class="product_page trivia_page" >

        <div class='background'></div>   
        <!-- trivia questions -->
        <div class="trivia_header">
            <h4>Click on puzzle piece to reveal questions. Answer all questions correctly to solve the trivia.</h4></div>
        <div class="puzzle" style="background-image:url(<?=FRONT_ASSETS?>img/ali_child.jpg)">
            
            <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1072.29 1590.78"><defs><style>.cls-1{fill:#c9aa6d;}.cls-2{fill:#fff200;}.cls-3{fill:#f4f4f4;}.cls-4{fill:#2e3444;}.cls-5{fill:#c63c25;}.cls-6{fill:#3f3ac2;}</style></defs>
              
                
  <!--           Top Left Piece     -->
    <path d="M559,415.21V260.68H22.66a18.81,18.81,0,0,0-18.81,18.8V815.81H175.77A18.81,18.81,0,0,0,194.58,797a86.84,86.84,0,1,1,173.68,0,18.81,18.81,0,0,0,18.81,18.81H559V661.28a124.47,124.47,0,0,0,0-246.07ZM521.37,643.89v134.3H404.46c-9.1-59.73-60.81-105.64-123-105.64a124.64,124.64,0,0,0-123,105.64H41.47V298.29h479.9V432.6a18.81,18.81,0,0,0,18.81,18.81,86.84,86.84,0,1,1,0,173.67A18.81,18.81,0,0,0,521.37,643.89Z" transform="translate(-3.85 -260.68)"/>
    <path class="cls-4 puzzle_click" id="1" d="M627,538.24a86.93,86.93,0,0,0-86.84-86.83,18.81,18.81,0,0,1-18.81-18.81V298.29H41.47v479.9H158.39a124.64,124.64,0,0,1,123-105.64c62.23,0,113.94,45.91,123,105.64H521.37V643.89a18.81,18.81,0,0,1,18.81-18.81A86.94,86.94,0,0,0,627,538.24Z" transform="translate(-3.85 -260.68)"/>              
                
    <!--           Top Right Piece     -->              
    <path d="M1057.34,260.68H521V432.6a18.82,18.82,0,0,0,18.81,18.81,86.84,86.84,0,1,1,0,173.67A18.81,18.81,0,0,0,521,643.89V815.81H675.54c9.1,59.73,60.81,105.65,123,105.65s113.94-45.92,123-105.65h154.54V279.48A18.81,18.81,0,0,0,1057.34,260.68Zm-18.81,517.51H904.23A18.81,18.81,0,0,0,885.42,797a86.84,86.84,0,1,1-173.68,0,18.81,18.81,0,0,0-18.8-18.81H558.63V661.28c59.73-9.09,105.65-60.81,105.65-123s-45.92-113.94-105.65-123V298.29h479.9v479.9Z" transform="translate(-3.85 -260.68)"/>
    <path class="cls-1 puzzle_click" id="2" d="M1038.53,298.29H558.63V415.21c59.73,9.09,105.65,60.8,105.65,123s-45.92,114-105.65,123V778.19H692.94A18.81,18.81,0,0,1,711.74,797a86.84,86.84,0,1,0,173.68,0,18.81,18.81,0,0,1,18.81-18.81h134.3V298.29Z" transform="translate(-3.85 -260.68)"/>

     
    <!--           Middle Left Piece     --> 
    <path d="M521.37,778.81H404.46c-9.1-59.73-60.81-105.65-123-105.65s-113.94,45.92-123,105.65H3.85v536.32a18.81,18.81,0,0,0,18.81,18.81H175.77a18.81,18.81,0,0,0,18.81-18.81,86.84,86.84,0,0,1,173.68,0,18.81,18.81,0,0,0,18.81,18.81H559V1162a18.81,18.81,0,0,0-18.8-18.81,86.84,86.84,0,1,1,0-173.67A18.81,18.81,0,0,0,559,950.73V778.81ZM415.72,1056.37a124.65,124.65,0,0,0,105.65,123v116.91H404.46c-9.1-59.73-60.81-105.64-123-105.64a124.65,124.65,0,0,0-123,105.65H41.47V816.43h134.3a18.81,18.81,0,0,0,18.81-18.81,86.84,86.84,0,0,1,173.68,0,18.81,18.81,0,0,0,18.81,18.81h134.3V933.34C461.64,942.43,415.72,994.14,415.72,1056.37Z" transform="translate(-3.85 -260.68)"/>          
    <path class="cls-6 puzzle_click" id="3" d="M521.37,816.43H387.07a18.81,18.81,0,0,1-18.81-18.81,86.84,86.84,0,0,0-173.68,0,18.81,18.81,0,0,1-18.81,18.81H41.47v479.9H158.38a124.65,124.65,0,0,1,123-105.65c62.23,0,113.94,45.91,123,105.64H521.37V1179.41a124.65,124.65,0,0,1-105.65-123c0-62.23,45.92-113.94,105.65-123Z" transform="translate(-3.85 -260.68)"/>
                
                
       <!--           Middle Right Piece     -->  
    <path d="M1038.53,778.19H904.23A18.81,18.81,0,0,0,885.42,797a86.84,86.84,0,1,1-173.68,0,18.81,18.81,0,0,0-18.8-18.81H521V932.72a124.47,124.47,0,0,0,0,246.07v154.53H675.55a124.47,124.47,0,0,0,246.07,0h135.72a18.81,18.81,0,0,0,18.81-18.8V778.19ZM885.42,1314.51a86.84,86.84,0,1,1-173.68,0,18.81,18.81,0,0,0-18.81-18.8H558.63V1161.4a18.81,18.81,0,0,0-18.81-18.8,86.84,86.84,0,0,1,0-173.68,18.81,18.81,0,0,0,18.81-18.81V815.81H675.54c9.1,59.73,60.81,105.65,123,105.65s113.94-45.92,123-105.65h116.92v479.9H904.23A18.81,18.81,0,0,0,885.42,1314.51Z" transform="translate(-3.85 -260.68)"/> 
      <path class="cls-5 puzzle_click" id="4" d="M1038.53,1178.79v-363H921.61c-9.09,59.73-60.8,105.65-123,105.65s-113.94-45.92-123-105.65H558.63v134.3a18.81,18.81,0,0,1-18.81,18.81,86.84,86.84,0,0,0,0,173.68,18.81,18.81,0,0,1,18.81,18.8v134.31h134.3a18.81,18.81,0,0,1,18.81,18.8,86.84,86.84,0,1,0,173.68,0,18.81,18.81,0,0,1,18.81-18.8h134.3Z" transform="translate(-3.85 -260.68)"/> 
                
        
                <!--           Lower Left Piece     --> 
   <path d="M521.37,1296.26H404.46c-9.1-59.73-60.81-105.65-123-105.65s-113.94,45.92-123,105.65H3.85v536.32a18.81,18.81,0,0,0,18.81,18.81H559V1679.47a18.81,18.81,0,0,0-18.8-18.81,86.84,86.84,0,1,1,0-173.67,18.81,18.81,0,0,0,18.8-18.81V1296.26ZM415.72,1573.83c0,62.23,45.92,113.94,105.65,123v116.92H41.47v-479.9h134.3a18.81,18.81,0,0,0,18.81-18.81,86.84,86.84,0,0,1,173.68,0,18.81,18.81,0,0,0,18.8,18.81H521.37v116.91C461.64,1459.88,415.72,1511.6,415.72,1573.83Z" transform="translate(-3.85 -260.68)"/>             
  <path class="cls-2 puzzle_click" id="5" d="M521.37,1333.88H387.06a18.81,18.81,0,0,1-18.8-18.81,86.84,86.84,0,0,0-173.68,0,18.81,18.81,0,0,1-18.81,18.81H41.47v479.9h479.9V1696.86c-59.73-9.09-105.65-60.8-105.65-123s45.92-113.95,105.65-123Z" transform="translate(-3.85 -260.68)"/>

         <!--           Lower Right Piece     -->        
    <path d="M1038.53,1296.33H904.23a18.81,18.81,0,0,0-18.81,18.8,86.84,86.84,0,1,1-173.68,0,18.81,18.81,0,0,0-18.81-18.8H521v154.53a124.47,124.47,0,0,0,0,246.07v154.53h536.33a18.81,18.81,0,0,0,18.81-18.81V1296.33Zm-479.9,171.91v-134.3H675.54c9.1,59.74,60.81,105.65,123,105.65a124.64,124.64,0,0,0,123-105.65h116.92v479.9H558.63v-134.3a18.81,18.81,0,0,0-18.81-18.81,86.84,86.84,0,1,1,0-173.68A18.81,18.81,0,0,0,558.63,1468.24Z" transform="translate(-3.85 -260.68)"/>
  <path class="cls-3 puzzle_click" id="6" d="M453,1573.89a86.94,86.94,0,0,0,86.84,86.84,18.81,18.81,0,0,1,18.81,18.81v134.3h479.9v-479.9H921.61a124.64,124.64,0,0,1-123,105.65c-62.23,0-113.94-45.91-123-105.65H558.63v134.3a18.81,18.81,0,0,1-18.81,18.81A86.94,86.94,0,0,0,453,1573.89Z" transform="translate(-3.85 -260.68)"/>
                


                
                



            </svg>
        </div>
    
        

        </section>  
        <script>
            $(".puzzle path.puzzle_click").click(function(){
                var id = $(this).attr('id');
                $(".question_holder").fadeOut(500);
                $(".question_holder:nth-child(" + id + ")").fadeIn(500);
            });
        </script>       
        <!-- trivia questions -->
          <section class="trivia_content trivia">
<!--
            <div class="title_holder">
              <p>1 / <?=count($questions)?></p>
            </div>
-->

            <!-- question 1 -->
            <?php foreach ($questions as $i => $question) {?>
                <div class="question_holder" data-id='<?= $i ?>' data-fail_text="<?=$question->failure_text?>" id="">
                    <div class="question">
<!--                        <p class="large_intro">QUESTION <?=$i+1?></p>-->
                        <p class="small_intro"><?=$question->text?></p>
                        <? if ($question->image != null && $question->image != '') {?>
<!--                            <img src="<?=UPLOAD_URL . 'questions/' . $question->image ?>" width="100"/>-->
                        <? } ?>
                    </div>

                    <? $choices = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' ?>

                    <?foreach ($question->answers as $a => $answer) {?>
                    <div class="answer click_action <?=$question->correct_answer_id == $answer->id ? 'correct_answer' : ''?>">
                        <h2><?=substr($choices,$a,1)?></h2>
                        <p><?=$answer->text?></p>
                        <? if ($answer->featured_image != null && $answer->featured_image != '') {?>
<!--                            <img src="<//?=UPLOAD_URL . 'answers/' . $answer->featured_image ?>" width="100"/>-->
                        <? } ?>
                    </div>
                    <? } ?>
<!--                    <a class="button submit">NEXT</a>-->
                </div>
            <? } ?>
          </section>
            <section class="trivia_completion trivia">
<!--
              <div class="title_holder">
                <h1>COGNIZANT TRIVIA</h1>
              </div>
-->

<!--
              <div class="question">
                <h2 class="large_intro"></h2>
                  <br>
                <p class="small_intro">Thank you for participating in the Trivia Challenge. Visit the Innovations Gallery for your participation prize and to be entered into our grand prize drawing on Wednesday.</p>
              </div>
-->

            </section>
        </section>  

</main>



<script>
$(document).ready(function () {
          let amount_right = 0
        $(document).on('click', '.answer', function(){

            let failure_text = $(this).parents('.question_holder').data('fail_text');
            
            // if correct answer
            if($(this).hasClass('correct_answer')){ 
              amount_right ++

              // remove ability to choose another answer
//                $(this).parents('.question_holder').children('.answer').each(function(){
//                    $(this).removeClass('answer click_action');
//                    $(this).addClass('answer_outcome');
//                });
                findPiece( $(this) );
                $(this).parents('.question_holder').addClass('corr_ans');

                var self = this;
                timer = setTimeout(function(){
                  $(self).parents('.question_holder').fadeOut(500);
                  lastQuestion();
                }, 500);

                var answers = $(this).parent('.question_holder').children('.answer_outcome');
                for ( i=0; i<answers.length; i++ ) {
                  if ( !$(answers[i]).hasClass('correct_answer') ) {
                    $(answers[i]).css('opacity', '0');
                    $(answers[i]).delay(2000).slideUp();
                  }
                }
              
//              $(this).after("<div class='answer_outcome correct'> <h2 class='right_answer'>CORRECT</h2></div>");
//              $('.correct').slideDown();
              // if ( failure_text !== "" ) {
              //   $(this).after("<div class='answer_outcome correct_info'></div>");
              //   $('.correct_info').append("<p>" + failure_text + "</p>");
              //   $('.correct_info').delay(2500).slideDown();
              // }

              $(this).parent('.question_holder').children('.submit').delay(2500).fadeIn('fast');
       

            // Wrong answer
            } else { 

                // remove ability to choose another answer
//                $(this).parents('.question_holder').children('.answer').each(function(){
//                    $(this).removeClass('answer click_action');
//                    $(this).addClass('answer_outcome');
//                });

                var answers = $(this).parent('.question_holder').children('.answer_outcome');
                for ( i=0; i<answers.length; i++ ) {
                  if ( !$(answers[i]).hasClass('correct_answer') ) {
                    $(answers[i]).css('opacity', '0');
                    $(answers[i]).delay(2000).slideUp();
                      
                  }
                }

                $(this).parent('.question_holder').addClass('wrong_ans');
                $(this).css('opacity', '0', 'pointer-events', 'none');
                var self = this;
                timer = setTimeout(function(){
                    $(self).parent('.question_holder').removeClass('wrong_ans');
                }, 500);

                $(this).parent('.question_holder').children('.submit').delay(2500).fadeIn('fast');
            }

        });


        function findPiece( answer ) {
          var id = parseInt($(answer).parents('.question_holder').attr('data-id')) +1;
          $('#' + id ).fadeOut(1000);
          $('#' + id ).prev('path').fadeOut(1000);

          var timer = setTimeout(function(){
            $('#' + id ).remove();
            if ( $('.puzzle_click').length < 1 ) {
              $('.trivia_header h4').html('CONGRATULATIONS');
              setTimeout(function(){
                window.location = '/home/kiosk1';
              }, 8000)
            }
          }); 
        }

        function lastQuestion(  ) {
        }


          // Next questions
          // var timer;
          // var question_num = 1
          // var total_questions = $('.title_holder p').html().substring($('.title_holder p').html().length - 2);
          // $(".submit").on({
          //      'click': function clickAction() {
          //          var self = this;
          //          if ( question_num === parseInt(total_questions)) {
          //             $('.trivia_content').css('padding-top', '200px');
          //             $('.trivia_content').fadeOut('fast');
          //          }else {
          //             $(self).parent('.question_holder').css('margin-top', '200px');
          //             $(self).parent('.question_holder').fadeOut('slow');
          //             console.log(amount_right)
          //          }

          //          console.log(question_num)
          //          parseInt(total_questions)

          //           timer = setTimeout(function () {
          //             if ( question_num === parseInt(total_questions)) {
          //               $('.trivia_completion').fadeIn('slow');
          //               $('.trivia_completion').css('padding-top', '0px');
          //               console.log(amount_right)
          //               $('.trivia_completion .question h2').html(amount_right + " / " + total_questions);
          //             }else {
          //               $(self).parent('.question_holder').next().fadeIn('slow');
          //               $(self).parent('.question_holder').next().css('margin-top', '0px');

          //               // Updating question number
          //               $('.title_holder p').html((question_num += 1).toString() + "  / " + total_questions )
          //             }

          //           }, 1000);
          //      }
          // });

               // Go home after trivia completion
         $('.button.submit').on('click', function(){
            setTimeout(function(){
                if ( $('.trivia_completion').css('display') == 'block' ) {
                    setTimeout(function(){
                        $('.trivia_completion').css('padding-top', '200px').fadeOut();
                    }, 49000);
                    setTimeout(function(){
                        window.location.replace("/");
                    }, 50000);
                 }
             }, 5000);
         });

         
    });

</script>



 