<div id='kiosk5' class='kiosk_check'></div>

<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<a class='home' href="/home/kiosk5"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>

<img id='close' src="<?= FRONT_ASSETS ?>img/x_dark.png" style='color: white;'>
<section class='slider'>
	<div class='slide text_slide' style="background-color: #3f3ac2">
		<div class='quotes'>
			<h4>Damon Hewitt</h4>
			<p>"CBMA IS A BEACON FOR A LOT OF THE FIELD. IT’S THE ONLY ORGANIZATION THAT, REGARDLESS OF ISSUE, MAKES ITSELF AVAILABLE TO ORGANIZATIONS ON THE GROUND TO ADVANCE THE CAUSE FOR BLACK MEN AND BOYS.THAT’S WHY IT HAS THOUSANDS OF INDIVIDUAL MEMBERS IN CONSTANT, ONGOING KINSHIP AND DISCOURSE.”</p>
			<p>- Damon Hewitt, Executive Director, Executives’ Alliance for Boys and Men of Color</p>
		</div>
	</div>

	<div class='slide text_slide' style="background-color: #c63c25">
		<div class='quotes'>
			<h4>Dr. Jackie Copeland-Carson</h4>
			<p>"JUST HAVING A BMA FIELD IS A MAJOR ACHIEVEMENT. I REMEMBER WHEN NOBODY WANTED TO TALK ABOUT BLACK MEN AND BOYS. EVEN IF THEY DID WANT TO TALK ABOUT IT, THEY WOULD SAY IT WAS A HOPELESS CAUSE.”</p>
			<p>- Dr. Jackie Copeland-Carson, AnitaBorg.org COO; Black Philanthropy Month Founder; Author, The My Brother’s Keeper Initiative: A Collaborative Philanthropy Strategy to Address America’s Black Male Crisis (2008)</p>
		</div>
	</div>

	<div class='slide text_slide' style="background-color: #caab69">
		<div class='quotes'>
			<h4>Trabian Shorters</h4>
			<p>"CBMA GAVE ME THE SPACE, ABILITY AND MORAL SUPPORT TO CREATE OUR ASSET-FRAMING PARADIGM, WHICH WE’RE USING TO EDUCATE PHILANTHROPIC LEADERS, EXECUTIVE LEADERS IN MAJOR FOUNDATIONS, SOCIAL INNOVATION FUNDS, MAJOR ASSOCIATIONS, LARGE CORPORATIONS, GOVERNMENT ENTITIES.”</p>
			<p>- Trabian Shorters, CEO & Founder, BMe Community</p>
		</div>
	</div>

	
</section>

<script type="text/javascript">

	$('.slider').slick({
	    arrows: true,
	    slidesToShow: 1,
	    autoplay: true,
	    focusOnSelect: false,
	    autoplaySpeed: 7000
	  });

	$('.action').click(function(){
		if ( $(this).hasClass('open') ) {
			$('button').show();
			$(this).parents('.slide').children('.quotes').slideUp();
			$(this).children('img').attr('src', '<?= FRONT_ASSETS ?>img/quote_dark.png');
			$(this).removeClass('open');
			$('.info h4').fadeIn();
			$('#close').fadeOut();
			
		}else {
			$('button').hide();
			$(this).parents('.slide').children('.quotes').slideDown();
			$(this).children('img').attr('src', '<?= FRONT_ASSETS ?>img/x_dark.png');
			$(this).addClass('open');
			$('.info h4').fadeOut();
			$('#close').fadeIn();
		}
	});

	$('#close').click(function(){
		$('button').show();
		$('.quotes').slideUp();
		$('.info h4').fadeIn();
		$('#close').fadeOut();
		$('.action').removeClass('open');
		$('.action').children('img').attr('src', '<?= FRONT_ASSETS ?>img/quote_dark.png');
	});
</script>